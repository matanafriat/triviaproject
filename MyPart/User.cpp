#include "User.h"

User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
}

void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* game)
{
	_currGame = game;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionNo, int questionTime)
{
	vector<User*> check; //check if user found in room
	_currRoom = new Room(roomId, this, roomName, maxUsers, questionNo, questionTime);
	check = _currRoom->getUsers();
	if (find(check.begin(), check.end(), this) != check.end()) //if user in existing users, room is created
	{
		send("[1140]"); //success
		return true;
	}
	send("[1141]"); //fail
	return false;
}

bool User::joinRoom(Room* room)
{
	if (_currRoom == nullptr && room->joinRoom(this)) //user not in a room and connected to room successfully
	{
		_currRoom = room;
		return true;
	}
	return false; //user in a room or connection to other room unsuccessfull
}

void User::leaveRoom()
{
	if (_currRoom != nullptr) //user in a room
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int roomId = -1;
	if (_currRoom != nullptr)
	{
		roomId = _currRoom->closeRoom(this);
		if (roomId != -1)
		{
			delete (_currRoom);
			_currRoom = nullptr;
		}
	}
	return roomId;
}

bool User::leaveGame()
{
	if (_currGame)
	{
		return(_currGame->leaveGame(_user));
	}
}