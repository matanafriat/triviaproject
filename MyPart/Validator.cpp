#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	if (password.length() < 4) //atleast 4 characters
	{
		return false;
	}
	if (password.find(' ') != string::npos)//checks for spaces
	{
		return false;
	}
	if (password.find_first_of("0123456789") == std::string::npos) //check for numbers
	{
		return false;
	}
	if (password.find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") == std::string::npos) //check for upper case letter
	{
		return false;
	}
	if (password.find_first_of("abcdefghijklmnopqrstuvwxyz") == std::string::npos) //check for lower case letter
	{
		return false;
	}
	return true; //all checks are successful! password is valid
}

bool Validator::isUsernameVaild(string username)
{
	if (username.length() == 0) //check string's length
	{
		return false;
	}
	if(!isalpha(username[0])) //check if first character is a letter
	{
		return false;
	}
	if (username.find(' ') != string::npos) //checks for spaces
	{
		return false;
	}
	return true; //all checks are successful! username is valid
}