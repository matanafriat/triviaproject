#pragma once
#include "Helper.h"
#include <string>
#include "Game.h"
#include "Room.h"
class Room;

using namespace std;

class User
{
public:
	User(string, SOCKET);
	void send(string);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* game);
	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};