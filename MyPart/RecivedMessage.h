#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <WinSock2.h>
#include "User.h"
using namespace std;
class RecivedMessage
{
public:
	RecivedMessage(SOCKET sock, int messageCode);
	RecivedMessage(SOCKET sock, int messageCode, vector<string> values);
	SOCKET getSock();
	User* getUser();
	void setUser(User* user);
	int getMessegeCode();
	vector<string>& getValues();
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};