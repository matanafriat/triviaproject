#pragma once
#include <string>

using namespace std;

class Validator
{
public:
	static bool isPasswordValid(string);
	static bool isUsernameVaild(string);
};
