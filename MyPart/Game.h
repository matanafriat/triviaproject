#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Question.h"
#include "DataBase.h"
#include "User.h"
using namespace std;
class Game
{
public:
	Game(const vector<User*>& players, int questionsNo,DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* user);
	int getID();
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionindex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _id;

};