#include "RecivedMessage.h"
RecivedMessage::RecivedMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}
RecivedMessage::RecivedMessage(SOCKET sock, int messageCode, vector<string> values)
{
	_sock = sock;
	_messageCode = messageCode;
	_values = values;
}

SOCKET RecivedMessage::getSock()
{
	return _sock;
}
User* RecivedMessage::getUser()
{
	return _user;
}
void RecivedMessage::setUser(User* user)
{
	_user = user;
}
int RecivedMessage::getMessegeCode()
{
	return _messageCode;
}
vector<string>& RecivedMessage::getValues()
{
	return _values;
}