#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int a2,a3,a4;
	_question = question;
	_id = id;
	_correctAnswerIndex = rand() % 4;
	_answers[_correctAnswerIndex] = correctAnswer;
	do
	{
		a2 = rand() % 4;
	}while (a2 == _correctAnswerIndex);
	_answers[a2] = answer2;
	do
	{
		a3 = rand() % 4;
	}while (a3 == _correctAnswerIndex || a3 == a2);
	_answers[a3] = answer3;
	a4 = 0;
	while (a4 == _correctAnswerIndex || a4 == a2 || a4 == a3);
	{
		a4++;
	};


}

string Question::getQuestion()
{
	return _question;
}

string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}
int Question::getId()
{
	return _id;
}