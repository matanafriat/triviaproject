#pragma once
#include <string>
#include <vector>
#include "User.h"
#include <exception>
class User;

using namespace std;

class Room
{
public:
	Room(int, User* , string, int, int, int);
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();
	
private:
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);

	vector<User*> _users;
	User* _admin;
	int _maxUsers, _questionTime, _questionsNo, _id;
	string _name;
};