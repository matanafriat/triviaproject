#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_users.insert(_users.begin(), admin);
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionNo;
	_questionTime = questionTime;
}

bool Room::joinRoom(User* user)
{
	if (_users.size() < _maxUsers) //max users not reached
	{
		_users.push_back(user);
		user->send("[1100 " + _questionsNo + ' ' + _questionTime + ']'); //success message
		sendMessage(getUsersListMessage()); //send message to users
		return true;
	}
	user->send("[1100 " + _questionsNo + ' ' + _questionTime + ']');
	return false;
}

void Room::leaveRoom(User* user)
{
	vector<User*>::iterator it = find(_users.begin(), _users.end(), user);
	if (it != _users.end())
	{
		_users.erase(it);
		user->send("[1120]"); //successfully left
		sendMessage(getUsersListMessage());
	}
}

int Room::closeRoom(User* user)
{
	if (user->getUsername().compare(_admin->getUsername()) == 0)
	{
		sendMessage("[116]");
		for (vector<User*>::iterator it = _users.begin(); it < _users.end(); it++)
		{
			if ((*it)->getUsername().compare(_admin->getUsername()) != 0) //not admin
			{
				(*it)->clearRoom();
			}
		}
		return _id;
	}
	return -1;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	string msg = "[108" + _users.size();
	if (_users.size() > 0) //if there are users
	{
		for (vector<User*>::iterator it = _users.begin(); it < _users.end(); it++)
		{
			msg += "##" + (*it)->getUsername();
		}
	}
	msg += ']';
	return msg;
}

int Room::getQuestionsNo()
{
	return _questionsNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

//private functions
string getUsersAsString(vector<User*>, User*);

void Room::sendMessage(string msg)
{
	if (_users.size() > 0) //if there are users
	{
		for (vector<User*>::iterator it = _users.begin(); it < _users.end(); it++)
		{
			(*it)->send(msg); //send msg
		}
	}
}

void Room::sendMessage(User* excludeUser, string msg)
{
	try
	{
		if (_users.size() > 0) //if there are users
		{
			for (vector<User*>::iterator it = _users.begin(); it < _users.end(); it++)
			{
				if ((*it)->getUsername().compare(excludeUser->getUsername()) != 0) //if the curr user isn't the exluded user
				{
					(*it)->send(msg); //send msg
				}
			}
		}
	}
	catch (exception ex)
	{
		printf(ex.what()); //error
	}
}

