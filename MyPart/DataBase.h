#pragma once
#include <string>
#include <vector>
#include <sqlite3.h>
#include <sstream>
#include <unordered_map>
#include "Question.h"
using namespace std;


class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionNo);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	int static callbackCount(void*, int, char**, char**);
	int static callback(void* notUsed, int argc, char** argv, char** azCol);
	int static callbackBestscores(void*, int, char**, char**);
	int static callbackPersonalStatus(void*, int, char**, char**);
private:
	sqlite3* _db;
	
};