#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db)
{
	int id;
	_db = db;
	_players = players;
	try
	{
		id = _db.insertNewGame();
		if (id == 0)
		{
			exception e;
			throw(e);
		}
	}
	
	catch (exception e)
	{
		cout << "ERROR, couldnt open game";
	}
	_questions = _db.initQuestions(questionsNo);
	_id = id;
}

Game::~Game()
{
	auto iter = _questions.begin();
	for (; iter <= _questions.end(); iter++)
	{
		free(*iter);
	}
	_questions.empty();
	_players.empty();
}

void Game::sendQuestionToAllUsers()
{
	stringstream s;
	std::vector<Question*>::size_type sz = _questions.size();
	char str[4];
	//try
	//{
		s << "118" << Helper::getPaddedNumber(sizeof(_questions.at(sz)->getQuestion()), 3) << _questions.at(sz)->getQuestion() << Helper::getPaddedNumber(sizeof(_questions.at(sz)->getAnswers()[0]), 3) << _questions.at(sz)->getAnswers[0] << Helper::getPaddedNumber(sizeof(_questions.at(sz)->getAnswers()[1]), 3) << _questions.at(sz)->getAnswers[1] << Helper::getPaddedNumber(sizeof(_questions.at(sz)->getAnswers()[2]), 3) << _questions.at(sz)->getAnswers[2] << Helper::getPaddedNumber(sizeof(_questions.at(sz)->getAnswers()[3]), 3) << _questions.at(sz)->getAnswers[3];
		for (int i = 0; i < _players.size(); i++)
		{
			_players[i]->send(s.str());
		}
	//}
	//if(was execption)
	//catch
		_questions.pop_back();
		_currentTurnAnswers = 0;

}
int Game::getID()
{
	return _id;
}

void Game::handleFinishGame()
{
	stringstream s;
	_db.updateGameStatus(_id);
	
		for (int i = 0; i < _players.size(); i++)
		{
			try
			{
				s << "121" << _players.size() << Helper::getPaddedNumber(_players.at(i)->getUsername().length(), 2) << _players.at(i)->getUsername() << _results.at(_players.at(i)->getUsername());
				_players.at(i)->send(s.str());
				_players.at(i)->setGame(nullptr);
			}
			catch (exception e)
			{
				_players.at(i)->send(e.what());
			}

		}
	
}
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}
bool Game::handleNextTurn()
{
	if (_players.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	else
	{
		if (_currentTurnAnswers == _players.size())
		{
			if (_currQuestionindex == _questions.at(0)->getId())
			{
				handleFinishGame();
			}
			_questions_no++;
			sendQuestionToAllUsers();
		}
	}
	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	stringstream s;
	int Rans;
	_currentTurnAnswers++;
	for (int i = 0; i < _questions.size(); i++)
	{
		if (_currQuestionindex == _questions.at(i)->getId())
		{
			Rans = _questions.at(i)->getCorrectAnswerIndex;
			break;
		}
	}
	int isright=0;
	if (answerNo == Rans)
	{
		_results.at(user->getUsername())++;
		isright = 1;
	}
	if (time == 5)
	{
		_db.addAnswerToPlayer(_id, user->getUsername(), _currQuestionindex, NULL, isright, time);
	}
	else
	{
		_db.addAnswerToPlayer(_id, user->getUsername(), _currQuestionindex, _questions.at(_currQuestionindex)->getAnswers()[Rans], isright, time);

	}
	s << "120" << isright;
	user->send(s.str());
	return (Game::handleNextTurn());

}
bool Game::leaveGame(User* currUser)
{
	auto it = find(_players.begin(), _players.end(), currUser);

	if ( it != _players.end())
	{
		_players.erase(it);
		handleNextTurn();
	}
	if (_players.size() > 0)
	{
		return true;
	}
	return false;
}

bool Game::insertGameToDB()
{
	// didnt uderstand what am i supposed to insert? everything is automaticly filled
}
void Game::initQuestionsFromDB()
{

}