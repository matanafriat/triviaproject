#include <iostream>
#include <string>
#include <vector>
#include <sqlite3.h>
#include <sstream>
#include <unordered_map>
#include "Question.h"
#include "DataBase.h"
using namespace std;

unordered_map<string, vector<string>> results;
int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

DataBase::DataBase()
{
	stringstream s;
	int rc;
	try
	{
		int a = sqlite3_open("db.db", &_db);
		if (a)
		{
			exception e;
			throw(e);
		}
		s << "create table if not exists users(username text primary key,password text,email text )";
		string str = s.str();
		rc = sqlite3_exec(_db, str.c_str(), callback, 0,0);
		s.str(string());
		s << "create table if not exists questions(question_id int primary key,question text,correct_ans text,ans2 text ,ans3 text ,ans4 text )";
		str = s.str();
		rc = sqlite3_exec(_db, str.c_str(), callback, 0, NULL);
		s.str(string());
		s << "create table if not exists games(game_id int autoincerment primary key  not null  ,status int ,start_time datetime ,end_time datetime )";
		 str = s.str();
		rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
		s.str(string());
		s << "create table if not exists players_answers(game_id int not null  ,username text not null  ,question_id int not null ,player_answer text not null  ,is_correct int not null , answer_time int not null  ,foreign key(game_id) references games(game_id),foreign key(username) references users(username),foreign key(question_id) references questions(question_id),primary key(game_id,username,question_id))";
		str = s.str();
		rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);


	}
	catch (exception e)
	{
		cout << "can't open database \n";
	}
}

bool DataBase::isUserExists(string username)
{
	stringstream s;
	s << "select username from users where username =" << username;
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	else
	{
		auto iter = results.begin();
		if (iter->second.empty())
		{
			return 0;
		}
		return 1;
	}
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	stringstream s;
	s << "select username from users where username =" << username;
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	else
	{
		auto iter = results.begin();
		if (iter->second.empty())
		{
			return 0;
		}
		else
		{
			clearTable();
			s.str(string());
			s << "select password from users where username =" << username;
			str = s.str();
			rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error";
				system("Pause");
				return 0;
			}
			iter = results.begin();
			if (strcmp(iter->second.at(0).c_str(), password.c_str()) == 0)
			{
				return 1;
			}
			return 0;
		}
	}

}

bool DataBase::addNewUser(string username, string password, string email)
{
	stringstream s;
	s << "insert into users(username,password,email) values(" << username << "," << password << "," << email << ")";
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	return 1;
}

vector<Question*> DataBase::initQuestions(int questionNo)
{
	stringstream s;
	int a;
	auto iter = results.begin();
	//int* arr = new int[questionNo];
	vector<Question*> questions;
	vector<Question*>::iterator it = questions.begin();
	int arr[10] = { -1 };
	for (int i = 0; i < 10; i++)
	{
		do
		{
			a = rand() % 10;
		} while (a == arr[0] || a == arr[1] || a == arr[2] || a == arr[3] || a == arr[4] || a == arr[5] || a == arr[6] || a == arr[7] || a == arr[8] || a == arr[9]); //ill do it smart really soon
		arr[i] = a;
	}
	for (int i = 0; i < questionNo;i++)
	{
		s << "select * from questions where id =" << arr[i];
		string str = s.str();
		int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error";
			system("Pause");
			//return -1;
		}

		//for (int j = 0;j<)
		Question* a = new Question(atoi(iter->second.at(0).c_str()), iter->second.at(1), iter->second.at(2), iter->second.at(3), iter->second.at(4), iter->second.at(5));
		questions.insert(it,a);
		it++;
	}
	return questions;
	
}
/*vector<string> DataBase::getBestScores()
{
	
}*/

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	stringstream s;
	s << "insert into players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) values(" << gameId << "," << username << "," << questionId << ","<<answer<<","<<isCorrect<<","<<answerTime<<")";
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	return 1;
}

bool DataBase::updateGameStatus(int gameId)
{
	stringstream s;
	s << "update games set status=1,end_time='now' where game_id =" << gameId<<")";
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	return 1;
}

int DataBase::insertNewGame()
{
	stringstream s;
	s << "insert into games(status,start_time) values(0,now)";
	string str = s.str();
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, 0);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	clearTable();
	s.str(string());
	s << "select game_id from games where status=0";
	str = s.str();
	rc = sqlite3_exec(_db, str.c_str(), callback,0,NULL);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error";
		system("Pause");
		return 0;
	}
	auto iter = results.begin();
	if (!iter->second.empty())
	{
		int num = atoi(iter->second.at(0).c_str());
		return num;

	}
	else
	{
		cout << "ERROR!";
		return 0;
	}

	
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}
int main()
{
	DataBase a;
	a.initQuestions(5);
	return 0;
}